# TempoTem - Teste Front-End

Olá caro desenvolvedor front-end, nesse teste analisaremos seu conhecimento básico e avançado sobre as tecnologias que utilizamos. Abaixo explicaremos tudo o que será necessário, lembrando que, "clean code" e foco em performance, serão pontos positivos na sua avaliação.

## Descrição do teste
Essa avaliação consiste em testar seus conhecimentos de HTML, CSS, SASS, JavaScript, Vue, REST e etc. Você será responsável por desenvolver um layout simples baseado no layout da NetFlix

O tempo ideal para realização da tarefa é de **3 dias**

* Prazo ideal: 3 dias

**front-end-test-page-01**
![picture](assets/test_front.png)

## O que você precisará fazer
Não aceitaremos pacote .zip, .rar ou qualquer outra extensão/arquivo que não seja exposta publicamente via GIT.

* Criar um projeto em VUE com 2 rotas: Inicio, Favorito
* A Rota de inicio deverá conter 4 seções de carrossel com as categorias: "Gatos", "Esportes", "Chocolate", "Tecnologia"
* A url de requisição é: "http://api.giphy.com/v1/gifs/search?api_key=dc6zaTOxFJmzC&q=gatos"
* A última palavra da URl deve ser trocada pela Seção desejada, exemplo: "http://api.giphy.com/v1/gifs/search?api_key=dc6zaTOxFJmzC&q=Esportes"
* O Card da Seção de Gifs, ao colocar o mouse por cima de um gif, deverá conter a opção: "Adicionar aos favoritos"
* Utilizar o "VueX" para guardar os itens adicionados aos favoritos
* A Rota de favoritos deverá conter uma lista de "gifs" que foram adicionados aos favoritos
* Também deve ser possível remover dos favoritos caso deseje.

## Quais tecnologias deve usar
Atente-se a essas requisições, a falta de uma ou mais delas será motivo de penalização.

* Vue como framework principal
* Vuex como gerenciador de estado global
* Axios para realizar as requisições
* SASS
* Swipper para o Carrossel


## Pontos extras

* Mobile First
* TDD (Opcional)
* Pipeline || Docker file
* Gitflow
* Código limpo, afinal, um bom Dev nao é aquele que escreve códigos dificeis, mas sim aquele que escreve códigos legíveis
* Todo e qualquer detalhe de capricho será analisado.

## Critérios de aceite

* Deploy da página no Bitbucket Pages
* Link do Git onde a aplicação está no footer do projeto


Ao finalizar o teste encaminhar URL para o e-mail: ter_leonardo.luiz@tempotem.com.br

